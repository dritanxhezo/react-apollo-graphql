import React, {Component} from 'react';
import Article from '../article/article';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import './article-list.css';

export const ARTICLES_QUERY = gql`query articles{
        articles {
            id
            title
            author {
                id
                username
            }
            comments {
                text
                author {
                    id
                    username
                }
            }
        }
    }`;

class ArticleList extends Component {
    render() {
        return (
            <Query query={ARTICLES_QUERY}>
                {({loading, error, data}) => {
                    if (loading) return <div>Fetching</div>;
                    if (error) return <div>Error</div>;

                    const articlesToRender = data.articles;
                    return <div>{articlesToRender.map(article => <Article key={article.id} article={article} />)}</div>;
                }}

            </Query>
        );
    }
}

export default ArticleList;