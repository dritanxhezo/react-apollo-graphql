import React from 'react';
import logo from "../../logo.svg";
import "./header.css";
import {Link} from "react-router-dom";
import {withRouter} from "react-router";

const Header = () => {
    return (
        <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <div>Bunch of Articles</div>
            <div className="App-links">
                <Link to="/" className="App-link">articles</Link>
                <div className="divider"> | </div>
                <Link to="/new" className="App-link">new</Link>
            </div>
        </header>
    );
};

export default withRouter(Header);