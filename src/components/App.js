import React from 'react';
import './App.css';
import ArticleList from './article-list/article-list';
import NewArticle from "./new-article/new-article";
import Header from "./header/header";
import {Route, Switch} from "react-router";

function App() {
    return (
        <div className="App">
            <Header/>
            <Switch>
                <Route exact path="/" component={ArticleList} />
                <Route exact path="/new" component={NewArticle} />
            </Switch>

        </div>
    );
}

export default App;
