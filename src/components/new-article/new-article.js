import React, {Component} from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { ARTICLES_QUERY } from "../article-list/article-list";
import "./new-article.css";

const POST_MUTATION = gql`
    mutation PostMutation($title: String!, $authorId: Int!, $content: String!) {
        createArticle(input: {title: $title, authorId: $authorId, content: $content}) {
            id
            title
            author {
                id
                username
            }
            comments {
                text
                author {
                    id
                    username
                }
            }
        }
    }
`;

class NewArticle extends Component {
    state = {
        title: '',
        authorId: 0,
        content: ''
    };

    render() {
        const { title, authorId, content } = this.state;

        return (
            <div>
            <div className={"input-form"}>
                <input className="title" type="text"
                    value={title}
                    onChange={e => this.setState({ title: e.target.value })}
                    placeholder="A title for the article"
                />
                <input className="author" type="text"
                    value={authorId}
                    onChange={e => this.setState({ authorId: e.target.value })}
                    placeholder="The author of the article"
                />
                <input className="content" type="text"
                       value={content}
                       onChange={e => this.setState({ content: e.target.value })}
                       placeholder="The content of the article"
                />
            </div>
            <Mutation mutation={POST_MUTATION}
                      variables={{ title, authorId, content }}
                      onCompleted={() => this.props.history.push('/')}
                      update={(store, { data: { createArticle } }) => {
                          const data = store.readQuery({ query: ARTICLES_QUERY })
                          data.articles.push(createArticle)
                          store.writeQuery({
                              query: ARTICLES_QUERY,
                              data
                          })
                      }}
            >
                {postMutation => <button onClick={postMutation}>Submit</button>}
            </Mutation>
            </div>
        );
    }
}

export default NewArticle;