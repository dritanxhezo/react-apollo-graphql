import React, {Component} from 'react';
import './article.css';

class Article extends Component {
    render() {
        return (
            <div>
                {this.props.article.title} ({this.props.article.author.username}): [{this.props.article.comments.length} comments]
            </div>
        );
    }
}

export default Article;