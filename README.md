## Creating a graphQL client with React and Apollo

#### Create the React application

```
npx create-react-app react-apollo-graphql
cd react-apollo-graphql
yarn start
```
This will open a browser and navigate to http://localhost:3000 where the app is running. If everything went well, you’ll see the following:
![image info](./assets/Yujwwi6.png "React app running")

Move App.js and App.css, App.test.js to src/components

#### Install Apollo client

```
npm install --save apollo-client apollo-cache-inmemory apollo-link-http apollo-link-error apollo-link-state graphql-tag react-apollo graphql
```

Here’s an overview of the packages you just installed:
* **apollo-client:** Where all the magic happens
* **apollo-cache-inmemory:** Our recommended cache
* **apollo-link-http:** An Apollo Link for remote data fetching
* **apollo-link-error:** An Apollo Link for error handling
* **apollo-link-state:** An Apollo Link for local state management
* **graphql-tag:** Exports the gql function for your queries & mutations

(Instaed of these packages, you can also install **apollo-boost** that offers some convenience by bundling all these packages)

* **react-apollo** contains the bindings to use Apollo Client with React.
* **graphql** contains Facebook’s reference implementation of GraphQL - Apollo Client uses some of its functionality as well.

#### Configure ApolloClient
Apollo abstracts away all lower-level networking logic and provides a nice interface to the GraphQL server. In contrast to working with REST APIs, you don’t have to deal with constructing your own HTTP requests any more - instead you can simply write queries and mutations and send them using an ApolloClient instance.

The first thing you have to do when using Apollo is configure your ApolloClient instance. It needs to know the endpoint of your GraphQL API so it can deal with the network connections.

Open ```src/index.js``` and make these changes:
```
// 1
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

// 2
const httpLink = createHttpLink({
  uri: 'http://localhost:8080/graphql'
})

// 3
const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache()
})

// 4
ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root')
)
serviceWorker.unregister();
```
Let’s try to understand what’s going on in that code snippet:

1- You’re importing the required dependencies from the installed packages.

2- Here you create the httpLink that will connect your ApolloClient instance with the GraphQL API, your GraphQL server will be running on http://localhost:8080/graphql. This is where the Spring boot graphql will be served by **graphql-spring-boot-starter**

3- Now you instantiate ApolloClient by passing in the httpLink and a new instance of an InMemoryCache.

4- Finally you render the root component of your React app. The App is wrapped with the higher-order component ApolloProvider that gets passed the client as a prop.

That’s it, you’re all set to start for loading some data into your app! 

### Queries: Loading Links

#### Preparing the React components
The first piece of functionality you’ll implement in the app is loading and displaying a list of Article elements. You’ll walk up our way in the React component hierarchy and start with the component that’ll render a single link.
```
import React, {Component} from 'react';
import './article.css';

class Article extends Component {
    render() {
        return (
            <div>
                {this.props.article.title} ({this.props.article.author.username}): [{this.props.article.comments.length} comments]
            </div>
        );
    }
}

export default Article;
```

Next, you’ll implement the component that renders a list of articles.

```
import React, {Component} from 'react';
import Article from '../article/article';
import './article-list.css';

class ArticleList extends Component {
    render() {
        const articlesToRender = [
            {
                "id": 1,
                "title": "Hello world",
                "author": {
                    "id": 2,
                    "username": "dxhezo"
                },
                "comments": [
                    {
                        "text": "Do you like this article?",
                        "author": {
                            "id": 2,
                            "username": "dxhezo"
                        }
                    },
                    {
                        "text": "This is a great article",
                        "author": {
                            "id": 3,
                            "username": "admin"
                        }
                    }
                ]
            },
            {
                "id": 2,
                "title": "Foo",
                "author": {
                    "id": 3,
                    "username": "admin"
                },
                "comments": [
                    {
                        "text": "This is a comment",
                        "author": {
                            "id": 3,
                            "username": "admin"
                        }
                    }
                ]
            }
        ];

        return (
            <div>{articlesToRender.map(article => <Article key={article.id} article={article} />)}</div>
        );
    }
}

export default ArticleList;
```

Here, you’re using local mock data for now to make sure the component setup works. You’ll soon replace this with some actual data loaded from the server.

To complete, add ArticleList to App.js:
```
import React from 'react';
import logo from '../logo.svg';
import './App.css';
import ArticleList from './article-list/article-list';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <ArticleList/>
    </div>
  );
}

export default App;
```

#### Writing the GraphQL query
Next you’ll load the actual links that are stored in the database. The first thing you need to do for that is define the GraphQL query you want to send to the API.

Here is what it looks like:
```
query articles{
    articles {
        id
        title
        author {
            id
            username
        }
        comments {
            text
            author {
                id
                username
            }
        }
    }
}
```


#### Queries with Apollo Client
When using Apollo, you’ve got two ways of sending queries to the server.

The first one is to directly use the query method on the ApolloClient directly. This is a very direct way of fetching data and will allow you to process the response as a promise.

A practical example would look as follows:
```
 client.query({
  query: gql`
    {articles {
        id
        title
    }}
 }
  `
}).then(response => console.log(response.data.articles))
```
A more declarative way when using React however is to use new Apollo’s render prop API to manage your GraphQL data just using components.

With this approach, all you need to do when it comes to data fetching is pass the GraphQL query as prop and <Query /> component will fetch the data for you under the hood, then it’ll make it available in the component’s render prop function.

In general, the process for you to add some data fetching logic will be very similar every time:

* write the query as a JavaScript constant using the gql parser function
* use the <Query /> component passing the GraphQL query as prop
* access the query results that gets injected into the component’s render prop function

Open article-list and add the query like this:
```
...
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

const ARTICLES_QUERY = gql`query articles{
        articles {
            id
            title
            author {
                id
                username
            }
            comments {
                text
                author {
                    id
                    username
                }
            }
        }
    }`;

class ArticleList extends Component {
    render() {
        return (
            <Query query={ARTICLES_QUERY}>
                {({loading, error, data}) => {
                    if (loading) return <div>Fetching</div>;
                    if (error) return <div>Error</div>;

                    const articlesToRender = data.articles;
                    return <div>{articlesToRender.map(article => <Article key={article.id} article={article} />)}</div>;
                }}

            </Query>
        );
    }
}

export default ArticleList;
```
What’s going on here?

* First, you create the JavaScript constant called FEED_QUERY that stores the query. The gql function is used to parse the plain string that contains the GraphQL code (if you’re unfamiliar with the backtick-syntax, you can read up on JavaScript’s tagged template literals).
* Finally, you wrap the returned code with <Query /> component passing FEED_QUERY as prop.
* We've removed the mock data and render actual links that are fetched from the server thanks to <Query /> render prop function.

As expected, Apollo injected several props into the component’s render prop function. These props themselves provide information about the state of the network request:

* **loading:** Is true as long as the request is still ongoing and the response hasn’t been received.
* **error:** In case the request fails, this field will contain information about what exactly went wrong.
* **data:** This is the actual data that was received from the server. It has the links property which represents a list of Link elements.

That’s it! Now you should see data coming from the server.

### Mutations: Creating Links

In this section, you’ll learn how you can send mutations with Apollo. It’s actually not that different from sending queries and follows the same three steps that were mentioned before, with minor (but logical) differences in the last two steps:

* write the mutation as a JavaScript constant using the gql parser function
* use the <Mutation /> component passing the GraphQL mutation and variables (if needed) as props
* use the mutation function that gets injected into the component’s render prop function

#### Preparing the React components
Like before, let’s start by writing the React component where users will be able to add new links.

Create a new file in the src/components/new-article directory and call it new-article.js. Then paste the following code into it:

..src/components/new-article/new-article.js
```
import React, {Component} from 'react';

class NewArticle extends Component {
    state = {
        title: '',
        authorId: 0,
        content: ''
    };

    render() {
        const { title, authorId, content } = this.state;

        return (
            <div>
            <div className={"input-form"}>
                <input className="title" type="text"
                    value={title}
                    onChange={e => this.setState({ title: e.target.value })}
                    placeholder="A title for the article"
                />
                <input className="author" type="text"
                    value={authorId}
                    onChange={e => this.setState({ authorId: e.target.value })}
                    placeholder="The author of the article"
                />
                <input className="content" type="text"
                       value={content}
                       onChange={e => this.setState({ content: e.target.value })}
                       placeholder="The content of the article"
                />
            </div>
            <button onClick={`... you'll implement this'}>Submit</button>
            </div>
        );
    }
}

export default NewArticle;
```

This is a standard setup for a React component with three input fields where users can provide the title, author and content of the Article they want to create. The data that’s typed into these fields is stored in the component’s state and will be used when the mutation is sent.

#### Writing the mutation
But how can you now actually send the mutation to your server? Let’s follow the three steps from before.

First you need to define the mutation in your JavaScript code and wrap your component with the graphql container. You’ll do that in a similar way as with the query before.

In new-article.js, add the following statement to the top of the file:

...src/components/new-article/new-article.js
```
const POST_MUTATION = gql`
    mutation PostMutation($title: String!, $authorId: Int!, $content: String!) {
        createArticle(input: {title: $title, authorId: $authorId, content: $content}) {
            id
            title
            author {
                username
                bio
            }
            content
        }
    }
`;
```
Also replace the current button with the following:

...src/components/new-article/new-article.js
```
<Mutation mutation={POST_MUTATION} variables={{ title, authorId, content }}>
  {() => (
    <button onClick={`... you'll implement this`}>
      Submit
    </button>
  )}
</Mutation>
```
Let’s take a closer look again to understand what’s going on:

1. You first create the JavaScript constant called POST_MUTATION that stores the mutation.
2. Now, you wrap the button element as render prop function result with <Mutation /> component passing POST_MUTATION as prop.
3. Lastly you pass description and url states as variables prop.

Before moving on, you need to import the Apollo dependencies. Add the following to the top of CreateLink.js:

...src/components/new-article/new-article.js
```
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'
```
Let’s see the mutation in action!

Still in new-article.js, replace <Mutation /> component as follows:

...src/components/new-article/new-article.js
```
<Mutation mutation={POST_MUTATION} variables={{ title, authorId, content }}>
  {postMutation => <button onClick={postMutation}>Submit</button>}
</Mutation>
```
As promised, all you need to do is call the function that Apollo injects into <Mutation /> component’s render prop function inside onClick button’s event.

Go ahead and see if the mutation works. To be able to test the code, open App.js and change render to look as follows:

...src/components/App.js
```
render() {
  return <NewArticle />
}
```
Next, import the NewArticle component by adding the following statement to the top of App.js:

...src/components/App.js
```
import NewArticle from "./new-article/new-article";
```

Now, run yarn start, you’ll see the following screen:
![image info](./assets/AJNlEfj.png "new article")

Two input fields and a submit-button - not very pretty but functional.

Enter some data into the fields, e.g.:

Title: The best learning resource for GraphQL
AuthorId: 0
Content: something
Then click the submit-button. You won’t get any visual feedback in the UI, but let’s see if the query actually worked by checking the current list of articles in graphiql.
```
# Try to write your query here
query AllArticles {
  articles {
    id
    title
    content
    author {
      id
      username
    }
    comments {
      text
      author {
        id
        username
      }
    }
  }
}
```
You’ll see the following server response:
```
{
  "data": {
    "articles": [
      {
        "id": 1,
        "title": "Hello wold",
        "content": "This is a hello world",
        "author": {
          "id": 2,
          "username": "dxhezo"
        },
        "comments": [
          {
            "text": "Do you like this article?",
            "author": {
              "id": 2,
              "username": "dxhezo"
            }
          },
          {
            "text": "This is a great article",
            "author": {
              "id": 3,
              "username": "admin"
            }
          }
        ]
      },
      ...
```
Awesome! The mutation works, great job!

### Routing

In this section, you’ll learn how to use the react-router library with Apollo to implement some navigation functionality!

#### Install dependencies

First add the required dependencies to the app. Open a terminal, navigate to your project directory and type:

```npm install --save react-router react-router-dom```

#### Create a Header
Before moving on to configure the different routes for your application, you need to create a Header component that users can use to navigate between the different parts of your app.

Create a new file in src/components and call it header.js. Then paste the following code inside of it:

...src/components/header/header.js
```
import React from 'react';
import logo from "../../logo.svg";
import "./header.css";
import {Link} from "react-router-dom";
import {withRouter} from "react-router";

const Header = () => {
    return (
        <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <div>Bunch of Articles</div>
            <div className="App-links">
                <Link to="/" className="App-link">articles</Link>
                <div className="divider"> | </div>
                <Link to="/new" className="App-link">new</Link>
            </div>
        </header>
    );
};

export default withRouter(Header);
```

This simply renders two Link components that users can use to navigate between the article-list and the new-article components.

#### Setup routes
You’ll configure the different routes for the app in the project’s root component: App.

Open the corresponding file App.js and update render to include the Header as well as LinkList and the CreateLink components under different routes:

.../src/components/App.js
```
function App() {
    return (
        <div className="App">
            <Header/>
            <Switch>
                <Route exact path="/" component={ArticleList} />
                <Route exact path="/new" component={NewArticle} />
            </Switch>

        </div>
    );
}
```
For this code to work, you need to import the required dependencies of react-router-dom.

Add the following statement to the top of the file:
```
import Header from "./header/header";
import {Route, Switch} from "react-router";
```
Now you need to wrap the App with BrowserRouter so that all child components of App will get access to the routing functionality.

Open index.js and add the following import statement to the top:

.../src/index.js
```
import { BrowserRouter } from 'react-router-dom'
```
Now update ReactDOM.render and wrap the whole app with the BrowserRouter:

.../src/index.js
```
ReactDOM.render(
    <BrowserRouter>
        <ApolloProvider client={client}>
            <App />
        </ApolloProvider>
    </BrowserRouter>,
    document.getElementById('root')
);
```
That’s it. If you run the app again, you can now access two URLs. http://localhost:3000/ will render ArticleList and http://localhost:3000/new renders the NewArticle component you just wrote in the previous section.

 
#### Implement navigation
To wrap up this section, you need to implement an automatic redirect from the NewArticle component to the ArticleList component after a mutation was performed.

Open NewArticle.js and update the <Mutation /> component to look as follows:

...src/components/new-article/new-article.js
```
<Mutation
  mutation={POST_MUTATION}
  variables={{ title, authorId, content }}
  onCompleted={() => this.props.history.push('/')}
>
  {postMutation => <button onClick={postMutation}>Submit</button>}
</Mutation>
```
After the mutation was performed, react-router-dom will now navigate back to the LinkList component that’s accessible on the root route: /.

Note: It won’t display the newly created Link, it’ll just redirect to the root route, you could always refresh to see the changes made. We’ll see how to update the data after the Mutation is being triggered on the More Mutations and Updating the Store chapter!

#### Updating the cache

Let’s implement the update for adding new articles!

Open NewArticle.js and following what we did before, update <Mutation /> component passing update as prop like so:

...src/components/new-article/new-article.js
```
<Mutation mutation={POST_MUTATION}
      variables={{ title, authorId, content }}
      onCompleted={() => this.props.history.push('/')}
      update={(store, { data: { createArticle } }) => {
          const data = store.readQuery({ query: ARTICLES_QUERY })
          data.articles.push(createArticle)
          store.writeQuery({
              query: ARTICLES_QUERY,
              data
          })
      }}
>
{postMutation => <button onClick={postMutation}>Submit</button>}
</Mutation>
```

 You first read the current state of the results of the ARTICLES_QUERY. Then you insert the newest link at beginning and write the query results back to the store.

The last thing you need to do for this to work is import the ARTICLES_QUERY into that file:

...src/components/new-article/new-article.js
```
import { ARTICLES_QUERY } from "../article-list/article-list";
```
Conversely, it also needs to be exported from where it is defined.

Open LinkList.js and adjust the definition of the FEED_QUERY by adding the export keyword to it:

...src/components/article-list/article-list.js
```
export const ARTICLES_QUERY = ...
```

Awesome, now the store also updates with the right information after new links are added.